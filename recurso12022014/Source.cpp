﻿#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include "estruturas.h"
#define LARGURA_TRONCO 0.2
#define ALTURA_TRONCO 0.4
#define ALTURA_COPA 0.3
#define RAIO_BOLA 0.03
#define DIST_OBJ 0.3
#define INC 5

Estado estado;
Arvore objArvore;
Bola objBola;

void init() {
	// cor do fundo
	glClearColor(0.63, 0.21, 0.66, 0.0);

	// inicializar o valores de visualização
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-1, 1, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void initEstado() {
	estado.delay = 100;
	estado.doubleBuffer = 1;
}

void initAnimacao() {
	objArvore.x = -DIST_OBJ - RAIO_BOLA;
	objArvore.y = ALTURA_TRONCO / 2.0;
	objArvore.angulo = 0.0;
	objArvore.caiu = GL_FALSE;
	objBola.x = -RAIO_BOLA;
	objBola.y = RAIO_BOLA;
	objBola.velocidade = -0.01;
}

void desenhaReferencialCartesiano()
{
	glColor3f(1.0, 1.0, 1.0);

	//x
	glBegin(GL_LINES);
	glVertex3f(0.0, -1.0, 0.0);
	glVertex3f(0.0, 1.0, 0.0);
	glEnd();

	//y
	glBegin(GL_LINES);
	glVertex3f(-1.0, 0.0, 0.0);
	glVertex3f(1.0, 0.0, 0.0);
	glEnd();
}

void triang() {
	glBegin(GL_TRIANGLES);
	glVertex2f(-1.0, -1.0);
	glVertex2f(1.0, -1.0);
	glVertex2f(0.0, 1.0);
	glEnd();
}

void box() {
	glBegin(GL_QUADS);
	glVertex2f(-1.0, -1.0);
	glVertex2f(1.0, -1.0);
	glVertex2f(1.0, 1.0);
	glVertex2f(-1.0, 1.0);
	glEnd();
}

void bola() {
	glutSolidSphere(1, 100, 100);
}

void desenhaArvore() {
	glPushMatrix();
		glPushMatrix();
			glScaled(LARGURA_TRONCO / 2.0, ALTURA_TRONCO / 2.0, 1.0);
			box();
		glPopMatrix();
		glTranslatef(0.0, ALTURA_TRONCO / 2.0 + ALTURA_COPA / 2.0, 0.0);
		glPushMatrix();
			glScaled(ALTURA_COPA / 2.0, ALTURA_COPA / 2.0, 1.0);
			triang();
		glPopMatrix();
	glPopMatrix();
}

void desenhaCena() {
	glClear(GL_COLOR_BUFFER_BIT);

	glPushMatrix();
	desenhaReferencialCartesiano();
	glPopMatrix();

	glPushMatrix();
		glPushMatrix();
			// função de desenho
			glTranslatef(objBola.x, objBola.y, 0.0);
			glScaled(RAIO_BOLA, RAIO_BOLA, 1.0);
			bola();
		glPopMatrix();
		//distancia entre objectos
		glTranslatef(-DIST_OBJ + RAIO_BOLA - (LARGURA_TRONCO / 4.0), 0.0, 0.0);
		// desenha arvore pequena
		glPushMatrix();
			glRotatef(objArvore.angulo, 0.0, 0.0, 1.0);
			glTranslatef((LARGURA_TRONCO / 4.0), (ALTURA_TRONCO / 4.0), 0.0);
			glScaled(0.5, 0.5, 1.0);
			desenhaArvore();
		glPopMatrix();
		glTranslatef(-DIST_OBJ - (LARGURA_TRONCO / 2.0) - (LARGURA_TRONCO / 2.0), 0.0, 0.0);
		// desenha arvore grande
		glPushMatrix();
			glTranslatef((LARGURA_TRONCO / 2.0), (ALTURA_TRONCO / 2.0), 0.0);
			desenhaArvore();
		glPopMatrix();
		glTranslatef(-DIST_OBJ - RAIO_BOLA - (LARGURA_TRONCO / 2.0), 0.0, 0.0);
	glPopMatrix();
	glFlush();

	if (estado.doubleBuffer) {
		glutSwapBuffers();
	}
}

void Timer(int value) {
	glutTimerFunc(estado.delay, Timer, 0);

	GLfloat nx = objBola.x + objBola.velocidade;

	cout << "pos arvore x: " << objArvore.x << endl;
	cout << "pos bola x: " << nx << endl;

	if (objArvore.caiu == GL_TRUE && objArvore.angulo < 90.0) {
		objArvore.angulo += INC;
	}
	else {
		if (nx - RAIO_BOLA < objArvore.x + (LARGURA_TRONCO / 2.0)) {
			objBola.velocidade = 0;
			objArvore.caiu = GL_TRUE;
			cout << "bateu!" << endl;
		}
		else {
			objBola.x = nx;
		}
	}

	glutPostRedisplay();
}

// CALLBACK PARA REDIMENSIONAR JANELA

void Reshape(int width, int height)
{
	GLint size;

	if (width < height)
		size = width;
	else
		size = height;

	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL

	glViewport(0, 0, (GLint)size, (GLint)size);


	// Matriz Projec��o
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// gluOrtho2D(left,right,bottom,top); 
	// projec��o ortogonal 2D, com profundidade (Z) entre -1 e 1
	gluOrtho2D(-1, 1, -1, 1);

	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformac�es dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB);

	glutInitWindowSize(500, 500);

	glutInitWindowPosition(100, 100);

	glutCreateWindow("casas");

	init();
	initEstado();
	initAnimacao();

	glutTimerFunc(estado.delay, Timer, 0);

	glutReshapeFunc(Reshape);
	glutDisplayFunc(desenhaCena);

	glutMainLoop();

	return 0;
}