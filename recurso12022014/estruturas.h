#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/gl.h>
#define _USE_MATH_DEFINES
#include <math.h>
#define DIST_PENDULOS 10
#define NUM_PENDULOS 2

using namespace std;

typedef struct Estado {
	GLint     delay;
	GLboolean doubleBuffer;
} Estado;

typedef struct Bola {
	GLfloat x;
	GLfloat y;
	GLfloat velocidade;
} Bola;

typedef struct Arvore {
	GLfloat x;
	GLfloat y;
	GLfloat angulo;
	GLboolean caiu;
} Arvore;